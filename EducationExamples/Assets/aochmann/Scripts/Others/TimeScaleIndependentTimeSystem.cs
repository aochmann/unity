﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.aochmann.Scripts.Others
{
    public class TimeScaleIndependentTimeSystem : MonoBehaviour
    {
        [SerializeField] private Text _text;
        private float _start;

        private void Awake()
        {
            if (_text == null)
                throw new NullReferenceException("Text not set");
            _start = Time.realtimeSinceStartup;
        }

        private void Update()
        {
            _text.text = DateTime.Now.ToString("HH:mm:ss");
        }
    }
}