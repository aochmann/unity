﻿using UnityEngine;

namespace Assets.aochmann.Scripts.Others
{
    [RequireComponent(typeof(CharacterController))]
    public class GoToPoint : MonoBehaviour, IGoToPoint
    {
        [SerializeField, Range(0f, 50f)] private float _moveSpeed = 2f;
        [SerializeField, Range(0f, 1f)] private float _distanceStopCheck = 0.5f;
        [SerializeField, Range(0f, 50f)] private float _rotationSpeed = 2f;

        private Vector3 _destination;
        private bool _initialized;
        private CharacterController _characterController;


        // Use this for initialization
        void Start()
        {
            _characterController = GetComponent<CharacterController>();
        }

        // Update is called once per frame
        void Update()
        {
            if (!_initialized) return;
            _destination.y = transform.position.y;
            Vector3 hedding = _destination - transform.position;
            Vector3 direction = hedding.normalized;

            if (Vector3.Distance(transform.position, _destination) <= _distanceStopCheck || hedding.magnitude <= _distanceStopCheck)
            {
                Debug.Log("I am here");
                GameObject.Destroy(gameObject);
            }

            var qTo = Quaternion.LookRotation(direction);

            transform.rotation = Quaternion.Slerp(transform.rotation, qTo, _rotationSpeed * Time.deltaTime);
            _characterController.Move(direction * _moveSpeed * Time.deltaTime);
        }

        public void SetDestination(Vector3 point)
        {
            _destination = point;
            _destination.y = transform.position.y;
            _initialized = true;
        }
    }
}