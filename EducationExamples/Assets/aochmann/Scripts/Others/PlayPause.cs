﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.aochmann.Scripts.Others
{
    public class PlayPause : MonoBehaviour
    {
        private float _previouseGameTimeScale = 0f;
        private IGameState _gameManager;

        private void Awake()
        {
            _gameManager = GameManager.Instance as IGameState;
        }

        public void ChangeGameState()
        {
            bool isGamePaused = _gameManager.IsGamePaused();
            ChangeGameLogicTime(isGamePaused);
            _gameManager.ChangeGameState();
        }

        private void ChangeGameLogicTime(bool value)
        {
            if (value)
                Time.timeScale = _previouseGameTimeScale;
            else
            {
                _previouseGameTimeScale = Time.timeScale;
                Time.timeScale = 0f;
            }
        }
    }
}
