using UnityEngine;

namespace Assets.aochmann.Scripts.Others
{
    public interface IGoToPoint
    {
        void SetDestination(Vector3 point);
    }
}