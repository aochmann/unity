﻿using UnityEngine;

namespace Assets.aochmann.Scripts.Others
{
    public class GameManager : MonoSingleton<MonoBehaviour>, IGameState
    {
        [SerializeField] private bool _gamePaused;
        [SerializeField] private UnityEngine.UI.Text[] _infoObjects;
        private readonly string _pausedText = "Pause Game";
        private readonly string _resumeText = "Resume Game";

        private void Start()
        {
            foreach (UnityEngine.UI.Text obj in _infoObjects)
            {
                obj.text = _gamePaused ?  _resumeText : _pausedText;
            }
        }

        public void ChangeGameState()
        {
            foreach (UnityEngine.UI.Text obj in _infoObjects)
            {
                obj.text = _gamePaused ? _pausedText : _resumeText;
            }

            _gamePaused = !_gamePaused;
        }

        public bool IsGamePaused()
        {
            return _gamePaused;
        }
    }
}