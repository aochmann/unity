namespace Assets.aochmann.Scripts.Others
{
    public interface IGameState
    {
        void ChangeGameState();
        bool IsGamePaused();
    }
}