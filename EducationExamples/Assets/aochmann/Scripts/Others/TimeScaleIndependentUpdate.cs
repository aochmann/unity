﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.aochmann.Scripts.Others
{
    [Serializable]
    public class TimeScaleIndependentUpdate : MonoBehaviour {

        [SerializeField]
        private bool _pauseWhenGameIsPaused = true;
        private float _previousTimeSinceStartup;
        private readonly string _deltaTimeWarning = "Delta time less than zero, discarding (delta time was {0})";
        private IGameState _gameStateManager;

        protected virtual void Awake()
        {
            _previousTimeSinceStartup = Time.realtimeSinceStartup;
        }

        protected virtual void Start()
        {
            _gameStateManager = GameManager.Instance as IGameState;
            Debug.Log(_gameStateManager == null);

            if(_gameStateManager == null)
                throw new NullReferenceException("Game Manager");
        }

        protected virtual void Update()
        {
            float realtimeSinceStartup = Time.realtimeSinceStartup;
            DeltaTime = realtimeSinceStartup - _previousTimeSinceStartup;
            _previousTimeSinceStartup = realtimeSinceStartup;

            //It is possible (especially if this script is attached to an object that is
            //created when the scene is loaded) that the calculated delta time is
            //less than zero.  In that case, discard this update.
            if(DeltaTime < 0)
            {
                Debug.LogWarning(string.Format(_deltaTimeWarning, DeltaTime));
                DeltaTime = 0f;
            }

            if(_pauseWhenGameIsPaused && _gameStateManager.IsGamePaused())
            {
                DeltaTime = 0f;
            }
        }

        public IEnumerator TimeScaleIndependentWaitForSeconds(float seconds)
        {
            float elapsedTime = 0;
            while(elapsedTime < seconds)
            {
                yield return null;
                elapsedTime += DeltaTime;
            }
        }

        public float DeltaTime { get; private set; }
    }
}
