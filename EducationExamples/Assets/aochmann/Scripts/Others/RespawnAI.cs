﻿using System.Collections;
using UnityEngine;

namespace Assets.aochmann.Scripts.Others
{
    public class RespawnAI : MonoBehaviour
    {
        [SerializeField] private GameObject _aiPrefab;
        [SerializeField] private Transform _pointsContainer;
        [SerializeField] private Transform _respawnPoint;
        [SerializeField, Range(0f, 10f)] private float _timeToWait = 1f;

        private Vector3[] _points;
        private int _index = 0;

        // Use this for initialization
        void Start()
        {
            _points = new Vector3[_pointsContainer.childCount];

            for (int pointIndex = 0; pointIndex < _points.Length; pointIndex++)
            {
                _points[pointIndex] = _pointsContainer.GetChild(pointIndex).position;
            }

            StartCoroutine(LoopLogic());
        }

        private IEnumerator LoopLogic()
        {
            while (true)
            {
                GameObject target = GameObject.Instantiate<GameObject>(_aiPrefab, _respawnPoint.position, transform.rotation, transform.parent);
                target.GetComponent<IGoToPoint>().SetDestination(_points[_index]);
                _index = (_index + 1) % _points.Length;
                yield return new WaitForSeconds(_timeToWait);
            }
            yield return 0;
        }


    }
}