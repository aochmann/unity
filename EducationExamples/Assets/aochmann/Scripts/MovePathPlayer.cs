﻿using System;
using System.Collections;
using System.Reflection;
using UnityEngine;


namespace Task5
{

    public class MovePathPlayer : MonoBehaviour
    {
        #region SERIALIZED FIELDS
        [SerializeField]
        [Range(0.1f, 140f)]
        private float _MoveDamping = 3f;
        [SerializeField]
        private Transform _ObjectA;

        [SerializeField]
        [Range(4f, 20f)]
        private float _DistanceToObject = 4f;

        public enum State
        {
            ChangingColor,
            Sleeping,
            Logging,
            PathSelecting,
            RandomSelectState
        }

        [SerializeField] private State _currentState = State.Sleeping;

        private Renderer _objectRenderTexture;
        private State[] _states;
        #endregion

        #region NON-SERIALIZED FIELDS
        #endregion
        // Use this for initialization

        private IEnumerator Start()
        {
            _states = Enum.GetValues(typeof(State)) as State[];
            _objectRenderTexture = GetComponent<Renderer>();

            IEnumerator selectedAction = null;

            while (true)
            {
                if (selectedAction == null || !selectedAction.MoveNext())
                {
                    yield return new WaitForSeconds(0.5f);

                    selectedAction = SetStateAction();
                    if (selectedAction != null)
                        yield return StartCoroutine(selectedAction);
                }
            }
        }

        private IEnumerator SetStateAction()
        {
            switch (_currentState)
            {
                case State.ChangingColor:
                    return ChangRandomColor();
                case State.Sleeping:
                    return WaitingXSeconds();
                case State.Logging:
                    return LoggSomeMsg();
                case State.PathSelecting:
                    return ReturnSomeRandomPathMethod();
                case State.RandomSelectState:
                    return RandomizeState();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IEnumerator RandomizeState()
        {
            int maxStatesCount = _states.Length - 1;

            _currentState = _states[UnityEngine.Random.Range(0, maxStatesCount)];

            yield return true;
        }

        private IEnumerator ReturnSomeRandomPathMethod()
        {

            IEnumerator selectedAction = null;

            switch (UnityEngine.Random.Range(0, 3))
            {
                //switch(3){
                case 2:
                    print("Go2Point");
                    selectedAction = GoToPoin(_ObjectA.position);
                    break;
                case 1:
                    print("GoToPointWithDistance");
                    selectedAction = GoToPointWithDistance(_ObjectA.position, _DistanceToObject);
                    break;
                case 0:
                    print("GoThroughPath");
                    Vector3[] pathPoints = GeneratePointsPathArray();
                    selectedAction = GoTroughPath(pathPoints);
                    break;
            }

            if (selectedAction != null)
                yield return StartCoroutine(selectedAction);

            while (!(selectedAction == null || !selectedAction.MoveNext()))
            {
                yield return 0;
            }

            Debug.Log("I am out");
            _currentState = State.ChangingColor;
            yield return 0;
        }

        private Vector3[] GeneratePointsPathArray()
        {
            var charPosition = transform.position;
            var objectPosition = _ObjectA.position;

            var distance = Vector3.Distance(charPosition, objectPosition);
            var pointCount = UnityEngine.Random.Range(10, (int)distance);
            var pathPoints = new Vector3[pointCount];

            for (int i = 0; i < pointCount; i++)
            {

                pathPoints[i] = Vector3.Slerp(charPosition, objectPosition, (float)i / (float)pointCount);
                pathPoints[i].y = charPosition.y;
            }

            _pointsPath = pathPoints;
            return pathPoints;
        }

        private IEnumerator LoggSomeMsg()
        {
            Debug.Log(string.Format("Some random text {0}", UnityEngine.Random.Range(0,10)));
            _currentState = State.PathSelecting;
            yield return 0;
        }

        private IEnumerator ChangRandomColor()
        {
            float timeDuration = 5;

            while (timeDuration >= 0)
            {
                _objectRenderTexture.material.color = GetRandomColor();
                yield return new WaitForSeconds(1.0f);
                timeDuration--;
            }

            _currentState = State.Sleeping;

            yield return 0;
        }

        private Color GetRandomColor()
        {
            return new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));
        }

        private Vector3[] _pointsPath;

        // Implement this OnDrawGizmosSelected if you want to draw gizmos only if the object is selected
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            var tmpPath = _pointsPath;

            if (tmpPath != null)
                foreach (var p in tmpPath)
                    Gizmos.DrawSphere(p, 0.5f);

        }

        private IEnumerator WaitingXSeconds()
        {
            float xSeconds = UnityEngine.Random.Range(1f, 5f);
            yield return new WaitForSeconds(xSeconds);
            print(string.Format("W8: {0} - end", xSeconds));
            _currentState = State.Logging;
            yield return 0;
        }

        private IEnumerator GoToPoin(Vector3 point)
        {
            float distance = 0f;
            do
            {
                var characterPosition = transform.position;
                var direction = (point - characterPosition).normalized;
                distance = Vector3.Distance(characterPosition, point);
                transform.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.deltaTime * _MoveDamping);
                yield return null;
            }
            while (distance >= 0.08f);
        }

        private IEnumerator GoToPointWithDistance(Vector3 point, float maxDistance)
        {

            float distance = 0f;

            do
            {

                var characterPosition = transform.position;
                var direction = (point - characterPosition).normalized;

                distance = Vector3.Distance(characterPosition, point);

                transform.position = Vector3.Lerp(characterPosition, characterPosition + direction, Time.deltaTime * _MoveDamping);
                yield return null;

            }
            while (distance - maxDistance >= 0.01f);

            print("Jest na miejscu");

        }

        private IEnumerator GoTroughPath(params Vector3[] path)
        {
            var arrayLength = path.Length;
            int index = 0;
            Vector3 direction;
            RaycastHit hit;
            Vector3 newPoint;

            while (index < arrayLength)
            {
                direction = path[index] - transform.position;
                direction.Normalize();
                if (Physics.Raycast(transform.position, direction, out hit, 0.8f))
                {
                    if (hit.collider.gameObject.tag != "objectFinishPlayer")
                    {

                        for (int i = index; i < arrayLength; i++)
                        {
                            newPoint = path[i];
                            if (Mathf.Abs(path[i].z) <= Mathf.Abs(hit.point.x))
                            {
                                newPoint.x = 2 * hit.point.x - path[i].x;
                            }
                            else
                            {
                                newPoint.z = 2 * hit.point.z - path[i].z;
                            }
                            path[i] = newPoint;
                        }
                    }

                }
                transform.position = Vector3.Lerp(transform.position, path[index], Time.deltaTime * _MoveDamping);
                if (Vector3.Distance(transform.position, path
                    [index]) <= 0.2f)
                {
                    index++;
                }

                yield return null;
            }

            _pointsPath = null;

            print("Jest na miejscu");

        }
    }
}