﻿using System;
using UnityEngine;

namespace Assets.aochmann.Scripts.HelpFunctions
{
    [ExecuteInEditMode]
    public class VectorRayDisplayer : MonoBehaviour
    {
        [SerializeField, Range(0f, 100f)]
        private float _rayLength = 2f;
        [SerializeField]
        private bool _visableUpVector = true;
        [SerializeField]
        private bool _visableDownVector = true;
        [SerializeField]
        private bool _visableLeftVector = true;
        [SerializeField]
        private bool _visableRightVector = true;
        [SerializeField]
        private bool _visableForwardVector = true;
        [SerializeField]
        private bool _visableBackVector = true;

        // Implement this OnDrawGizmosSelected if you want to draw gizmos only if the object is selected
        public void OnDrawGizmos()
        {
            Gizmos.color = Color.white;
            if (_visableUpVector)
                Gizmos.DrawRay(transform.position, transform.up * _rayLength);

            Gizmos.color = Color.red;
            if (_visableBackVector)
                Gizmos.DrawRay(transform.position, transform.forward * -1f * _rayLength);

            Gizmos.color = Color.green;
            if (_visableDownVector)
                Gizmos.DrawRay(transform.position, transform.up * -1f * _rayLength);

            Gizmos.color = Color.blue;
            if (_visableForwardVector)
                Gizmos.DrawRay(transform.position, transform.forward * _rayLength);

            Gizmos.color = Color.black;
            if (_visableRightVector)
                Gizmos.DrawRay(transform.position, transform.right * _rayLength);

            Gizmos.color = Color.magenta;
            if (_visableLeftVector)
                Gizmos.DrawRay(transform.position, transform.right * -1f * _rayLength);
        }
    }
}