﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.aochmann.Scripts.HelpFunctions
{
    public class ResetObjectPosition : MonoBehaviour
    {
        [SerializeField]
        private Transform[] _objectTransforms;
        private List<Transform> _objects = new List<Transform>();
        private List<Vector3> _objectsBackupPositions = new List<Vector3>();

        private void Awake()
        {
            Queue queueList = new Queue();

            foreach (Transform objectTransform in _objectTransforms)
            {
                queueList.Enqueue(objectTransform);

                while (queueList.Count > 0)
                {
                    Transform pointP = queueList.Dequeue() as Transform;

                    if (pointP == null) continue;
                    _objects.Add(pointP);
                    _objectsBackupPositions.Add(pointP.position);

                    if (pointP.childCount > 0)
                        for (int j = 0; j < pointP.childCount; j++)
                            queueList.Enqueue(pointP.GetChild(j));
                }
            }
        }

        public void OnGUI()
        {
            if (GUILayout.Button("Reset"))
            {
                for (int i = 0; i < _objects.Count; i++)
                {
                    _objects[i].position = _objectsBackupPositions[i];
                }
            }
        }
    }
}