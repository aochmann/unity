﻿using UnityEngine;
using System.Collections;

namespace Task5 {
    [RequireComponent(typeof(Rigidbody))]
    public class MoveObject : MonoBehaviour {

        #region SERIALIZED FIELDS

        [SerializeField]
        [Range(0.1f, 140f)]
        private float _MoveDamping = 8f;
        [SerializeField]
        [Range(0.1f, 100f)]
        private float _MaxDistance = 50;
        #endregion
        #region NON-SERIALIZED FIELDS
        private Rigidbody _CharacterRigidbody;
        private Vector3 _TargetPosition;
        private RaycastHit _RayHit;
        #endregion

        // Use this for initialization
        private void Start() {
            _CharacterRigidbody = GetComponent<Rigidbody>();
            _TargetPosition = transform.position;
        }

        // Update is called once per frame
        private void Update() {
            var characterPosition = transform.position;

            Debug.DrawLine(characterPosition, _TargetPosition, Color.black);

            if(Input.GetMouseButtonDown(0)) {

                var mousePosition = Input.mousePosition;

                var ray = Camera.main.ScreenPointToRay(mousePosition);


                if(Physics.Raycast(ray, out _RayHit, Mathf.Infinity)) {
                    _TargetPosition = _RayHit.point;
                    _TargetPosition.y += 0.5f;
                    var tmp = _TargetPosition - characterPosition;
                    tmp.y = 0;
                    transform.rotation = Quaternion.LookRotation(tmp);
                } //Raycast-if
            } //GetMouseButtonDown-if

            var heading     = _TargetPosition - characterPosition;
            var distance    = heading.magnitude;

            heading.y = 0;
            var direction = heading / distance; // This is now the normalized direction.
            direction.y = 0;

            if(distance >= 0.8f) {

                _CharacterRigidbody.AddForce(direction * _MoveDamping);
                
                Debug.DrawLine(characterPosition, characterPosition + direction * 0.9f, Color.green);
            } else {
                _CharacterRigidbody.velocity = Vector3.zero;
            }

        }
    }
}