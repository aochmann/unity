﻿using UnityEngine;

public class ObjectCollisionDetect : MonoBehaviour
{
    private Renderer _objectRenderer;


    private void Awake()
    {
        _objectRenderer = GetComponent<Renderer>();
    }

    private Color GetRandomColor()
    {
        return new Color(Random.Range(0f, 1f),
                            Random.Range(0f, 1f),
                                Random.Range(0f, 1f));
    }

    // OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider
    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision enter");
        _objectRenderer.material.color = collision.gameObject.GetComponent<Renderer>().material.color;
    }

    // OnCollisionExit is called when this collider/rigidbody has stopped touching another rigidbody/collider
    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("Collision exit");
        _objectRenderer.material.color = GetRandomColor();
    }

    // OnTriggerEnter is called when the Collider other enters the trigger
    public void OnTriggerEnter(Collider other)
    {
        _objectRenderer.material.color = other.gameObject.GetComponent<Renderer>().material.color;
    }

    public void OnTriggerExit(Collider other)
    {
        _objectRenderer.material.color = GetRandomColor();
    }
}