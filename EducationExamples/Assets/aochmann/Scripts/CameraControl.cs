﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    [SerializeField]
    [Range(0.1f, 100.0f)]
    private float _rotationSpeed = 4f;

    private void Update() {
        if (Input.GetKey(KeyCode.E))
            transform.Rotate(Vector3.forward, _rotationSpeed*Time.deltaTime);

        if (Input.GetKey(KeyCode.Q))
            transform.Rotate(Vector3.forward, -_rotationSpeed*Time.deltaTime);
    }

}