﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Inputs
{
    public class InputControllerGenerator
    {
        private readonly GameObject _gameObject;

        public InputControllerGenerator(GameObject gameObject)
        {
            _gameObject = gameObject;
        }

        public IInput CreateInputStrategy(bool clickToGo)
        {
            if (clickToGo)
                return _gameObject.AddComponent<ClickInput>();
            return _gameObject.AddComponent<KeyboardInput>();
        }
    }
}