﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Inputs
{
    public class KeyManager : IKeyInfo
    {
        public bool IsJumpingPressed()
        {
            return Input.GetKeyDown(KeyCode.Space);
        }

        public bool IsCrouchingPressed()
        {
            return Input.GetKey(KeyCode.C);
        }

        public bool IsRunningPressed()
        {
            return Input.GetKey(KeyCode.LeftShift);
        }
    }

    public interface IKeyInfo
    {
        bool IsRunningPressed();
        bool IsCrouchingPressed();
        bool IsJumpingPressed();
    }
}