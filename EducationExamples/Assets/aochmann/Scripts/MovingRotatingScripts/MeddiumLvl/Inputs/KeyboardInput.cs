﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Inputs
{
    internal class KeyboardInput : MonoBehaviour, IInput
    {
        public Vector3 GetDirection()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            return vertical * transform.forward + horizontal * Vector3.right;
        }

        public void Initialize(Camera camera)
        {
        }
    }
}