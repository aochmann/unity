using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Inputs
{
    public interface IInput
    {
        Vector3 GetDirection();
        void Initialize(Camera camera);
    }
}