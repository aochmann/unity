﻿using Assets.aochmann.Scripts.MovingScripts.Inputs;
using Assets.aochmann.Scripts.MovingScripts.Move;
using Assets.aochmann.Scripts.MovingScripts.Rotate;
using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts
{
    public class PlayerUserController : MonoBehaviour
    {
        [SerializeField]
        private MoveType _moveType = MoveType.Rigidbody;

        [SerializeField]
        private bool _useGravity = false;

        [SerializeField, Range(0f, 100f)]
        private float _walkSpeed = 2f;

        [SerializeField, Range(0f, 100f)]
        private float _runSpeed = 4f;

        [SerializeField, Range(0f, 100f)]
        private float _jumpHeight = 4f;

        [SerializeField]
        private RotationType _rotationType = RotationType.None;

        [SerializeField, Range(0f, 100f)]
        private float _mouseRotationSpeed = 2f;

        [SerializeField] private bool _clickToGo = false;

        private IMove _moveController;
        private bool _jumping;
        private IRotateStrategy _rotateController;
        private IInput _inputController;
        private IKeyInfo _keyManagerInfo;

        private void Awake()
        {
            _moveController = new MoveControllerGenerator(gameObject).CreateController(_moveType);
            _moveController.Initialize(_useGravity, _jumpHeight);

            _rotateController = new MouseRotationStrategyGenerator(gameObject).CreateRotateStrategy(_rotationType);
            _rotateController.Initialize(_mouseRotationSpeed);

            _inputController = new InputControllerGenerator(gameObject).CreateInputStrategy(_clickToGo);
            _inputController.Initialize(Camera.main);

            _keyManagerInfo = new KeyManager();
        }

        private void Update()
        {
            if (!_jumping)
            {
                _jumping = _keyManagerInfo.IsJumpingPressed();
                _moveController.Jump(_jumping);
            }
        }

        private void FixedUpdate()
        {
            Vector3 direction = _inputController.GetDirection();
            bool crouch = _keyManagerInfo.IsCrouchingPressed();

            if (_keyManagerInfo.IsRunningPressed())
                direction *= _runSpeed;
            else
                direction *= _walkSpeed;
            
            direction *= Time.fixedDeltaTime;

            _moveController.Move(direction);
            _rotateController.Rotate();
            _jumping = false;
        }

        //private void Rotate(Vector3 direction)
        //{
        //    if (direction == Vector3.zero) return;
        //    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.5f);
        //}

        // This function is called when the script is loaded or a value is changed in the inspector (Called in the editor only)
        public void OnValidate()
        {
            if (_runSpeed <= _walkSpeed)
                _runSpeed = _walkSpeed + 12f;
        }
    }
}
