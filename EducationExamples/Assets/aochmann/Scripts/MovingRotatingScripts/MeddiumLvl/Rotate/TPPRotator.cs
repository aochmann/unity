﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Rotate
{
    public class TPPRotator : MonoBehaviour, IRotateStrategy
    {
        public void Rotate()
        {
            //remember to rotate just eyes
            transform.Rotate(
               new Vector3(Input.GetAxis("Mouse Y"),
                               Input.GetAxis("Mouse X"),
                                   0)
               * Time.fixedDeltaTime
               * _rotationSpeed);
        }

        private float _rotationSpeed = 2;
        public void Initialize(float rotationSpeed)
        {
            _rotationSpeed = rotationSpeed;
        }
    }
}