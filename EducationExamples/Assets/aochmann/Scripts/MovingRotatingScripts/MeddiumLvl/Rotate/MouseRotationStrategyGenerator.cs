using System;
using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Rotate
{
    public class MouseRotationStrategyGenerator
    {
        private readonly GameObject _targetGameObject;

        public MouseRotationStrategyGenerator(GameObject gameObject)
        {
            _targetGameObject = gameObject;
        }

        public IRotateStrategy CreateRotateStrategy(RotationType rotationType)
        {
            IRotateStrategy rotate = null;

            switch (rotationType)
            {
                case RotationType.FirstPersonPerspective:
                    rotate = _targetGameObject.AddComponent<FPPRotator>();
                    break;

                case RotationType.Izometric:
                    throw new NotImplementedException();
                    break;
                case RotationType.UpLook:
                    rotate = _targetGameObject.AddComponent<UpLookRotator>();
                    break;
                case RotationType.SideLook:
                    throw new NotImplementedException();
                    break;
                case RotationType.ThirdPersonPerspective:
                    throw new NotImplementedException();
                    break;
                default:
                    rotate = _targetGameObject.AddComponent<NullObjectRotation>();
                    break;
            }

            return rotate;
        }
    }
}