﻿namespace Assets.aochmann.Scripts.MovingScripts.Rotate
{
    public interface IRotateStrategy
    {
        void Initialize(float rotationSpeed);
        void Rotate();
    }
}