﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Rotate
{
    public class FPPRotator : MonoBehaviour, IRotateStrategy
    {
        private float _rotationSpeed = 2;

        public void Rotate()
        {
            transform.Rotate(
                new Vector3(Input.GetAxis("Mouse Y"),
                                Input.GetAxis("Mouse X"),
                                    0) 
                * Time.fixedDeltaTime 
                * _rotationSpeed);
        }

        public void Initialize(float rotationSpeed)
        {
            _rotationSpeed = rotationSpeed;
        }
    }
}