﻿namespace Assets.aochmann.Scripts.MovingScripts.Rotate
{
    public enum RotationType
    {
        ThirdPersonPerspective,
        FirstPersonPerspective,
        Izometric,
        UpLook,
        SideLook,
        None
    }
}