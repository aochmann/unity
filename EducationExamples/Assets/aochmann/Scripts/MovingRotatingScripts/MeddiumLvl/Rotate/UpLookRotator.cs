﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Rotate
{
    public class UpLookRotator : MonoBehaviour, IRotateStrategy
    {
        public void Rotate()
        {

            //with Camera main integrate

            var mousePosition = Input.mousePosition;

            var ray = Camera.main.ScreenPointToRay(mousePosition);

            RaycastHit rayHit;
            if (Physics.Raycast(ray, out rayHit, Mathf.Infinity))
            {
                var tmp = rayHit.point - transform.position;
                tmp.y = 0;
                transform.rotation = Quaternion.LookRotation(tmp);
            }

            //transform.Rotate(
            //    new Vector3(0f, Input.GetAxis("Mouse X"),0f)
            //    * Time.fixedDeltaTime
            //    * _rotationSpeed);
        }

        private float _rotationSpeed = 2;
        public void Initialize(float rotationSpeed)
        {
            _rotationSpeed = rotationSpeed;
        }
    }
}