﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Rotate
{
    public class NullObjectRotation : MonoBehaviour, IRotateStrategy
    {
        public void Rotate() { }
        public void Initialize(float rotationSpeed) { }
    }
}