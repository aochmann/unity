﻿using System;
using Assets.aochmann.Scripts.MovingScripts.Move.Maths;
using Assets.aochmann.Scripts.MovingScripts.Move.Rigidbodies;
using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Move
{
    public class MoveControllerGenerator
    {
        private readonly GameObject _targetGameObject;

        public MoveControllerGenerator(GameObject gameObject)
        {
            _targetGameObject = gameObject;
        }

        public IMove CreateController(MoveType moveType)
        {
            IMove controller;

            switch (moveType)
            {
                case  MoveType.InterpollatedRigidbody:
                    controller = _targetGameObject.AddComponent<InterpollatedRigidbodyMoveController>();
                    break;
                case MoveType.Rigidbody:
                    controller = _targetGameObject.AddComponent<PlayerRigidbodyController>();
                    break;
                case MoveType.Math:
                    controller = _targetGameObject.AddComponent<MathCharacterController>();
                    break;
                case MoveType.CharacterController:
                    controller = _targetGameObject.AddComponent<PlayerCharacterController>();
                    break;
                default:
                    controller = _targetGameObject.AddComponent<NullObjectMoveController>();
                    break;

            }

            return controller;
        }
    }
}
