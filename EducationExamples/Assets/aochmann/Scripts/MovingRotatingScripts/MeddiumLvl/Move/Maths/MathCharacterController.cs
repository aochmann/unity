﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Move.Maths
{
    [RequireComponent(typeof(CapsuleCollider))]
    internal class MathCharacterController : MonoBehaviour, IMove
    {
        private readonly float _localMoveSpeed = 2f;

        public void Move(Vector3 direction)
        {
            Vector3 linearDestination = Vector3.Lerp(transform.position, transform.position + direction, Time.fixedDeltaTime * _localMoveSpeed);
            transform.position = new Vector3(linearDestination.x, transform.position.y, linearDestination.z);
        }

        public void Jump(bool jumping) { }

        public void Initialize(bool useGravity = false, float jumpHeight = 0) { }
    }
}
