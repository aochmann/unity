﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Move
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerCharacterController : MonoBehaviour, IMove
    {
        private CharacterController _objectCharacterController;
        private Vector3 _gravity;
        private bool _jumping;
        private float _jumpHeight;

        private void Start()
        {
            _objectCharacterController = GetComponent<CharacterController>();
        }

        public void Move(Vector3 direction)
        {
            if (_objectCharacterController.isGrounded && _jumping)
            {
                direction.y = _jumpHeight;
            }

            _objectCharacterController.Move(direction);
            _objectCharacterController.SimpleMove(_gravity);

            _jumping = false;
        }

        public void Jump(bool jumping)
        {
            _jumping = jumping;
        }

        public void Initialize(bool useGravity = false, float jumpHeight = 0)
        {
            if (useGravity)
                _gravity = Physics.gravity;
            _jumpHeight = jumpHeight;
        }
    }
}