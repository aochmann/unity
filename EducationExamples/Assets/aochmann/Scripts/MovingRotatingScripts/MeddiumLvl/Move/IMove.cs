﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Move
{
    public interface IMove
    {
        void Move(Vector3 direction);
        void Jump(bool jumping);
        void Initialize(bool useGravity = false, float jumpHeight = 0f);
    }
}