﻿namespace Assets.aochmann.Scripts.MovingScripts.Move
{
    public enum MoveType
    {
        Rigidbody,
        InterpollatedRigidbody,
        Math,
        CharacterController,
        Other
    }
}