﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Move
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public class PlayerRigidbodyController : MonoBehaviour, IMove
    {
        private Rigidbody _objectRigidbody;
        private bool _isPlayerGrounded;
        private Vector3 _targetPosition;
        private float _stopDistance = 1f;
        private CapsuleCollider _objectCapsule;
        private float _jumpingHeight;
        private bool _jumping;
        private float _gravityMultiplier = 4f;

        private void Awake()
        {
            _objectCapsule = GetComponent<CapsuleCollider>();
            _objectRigidbody = GetComponent<Rigidbody>();
            _objectRigidbody.constraints = RigidbodyConstraints.FreezeRotationX |
                                           RigidbodyConstraints.FreezeRotationZ;
        }

        public void Move(Vector3 direction)
        {
            _targetPosition = transform.position + direction;

            if (_targetPosition == Vector3.zero || _targetPosition == transform.position)
            {
                _objectRigidbody.velocity = Vector3.zero;
                return;
            }

            _isPlayerGrounded = FloorDownTest(transform.position + Vector3.down * _objectCapsule.radius);
            Vector3 playerActualVelocity = _objectRigidbody.velocity;

            if (Vector3.Distance(_targetPosition, transform.position) < _stopDistance)
            {
                _targetPosition = Vector3.zero;
                _objectRigidbody.velocity = Vector3.zero;
                return;

            }
            
            _objectRigidbody.AddForce(direction / Time.fixedDeltaTime * 10f, ForceMode.Acceleration );

            if (_isPlayerGrounded)
            {
                playerActualVelocity.y = Physics.gravity.y * _gravityMultiplier;

                if (_jumping)
                {
                    playerActualVelocity.y = _jumpingHeight;
                    _isPlayerGrounded = false;
                }
            }
            else
            {
                playerActualVelocity.y = Mathf.Clamp(playerActualVelocity.y + Physics.gravity.y * Time.fixedDeltaTime, Physics.gravity.y, _jumpingHeight);
            }

            _jumping = false;
            _objectRigidbody.velocity = playerActualVelocity;
        }

        public void Jump(bool jumping)
        {
            _jumping = jumping;
        }

        public void Initialize(bool useGravity = false, float jumpHeight = 0f)
        {
            _objectRigidbody.useGravity = useGravity;
            _jumpingHeight = jumpHeight;
        }
        
        private bool FloorDownTest(Vector3 direction)
        {
            RaycastHit hit;
            if (Physics.CapsuleCast(direction, direction, 0.5f, Vector3.down, out hit, 0.1f))
                if (Vector3.Dot(hit.normal, Vector3.up) > 0.5f)
                    return true;
            return false;
        }
    }
}
