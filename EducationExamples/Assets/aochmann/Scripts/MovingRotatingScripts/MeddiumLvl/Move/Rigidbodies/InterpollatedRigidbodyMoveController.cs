﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Move.Rigidbodies
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public class InterpollatedRigidbodyMoveController : MonoBehaviour, IMove
    {
        private Rigidbody _objectRigidbody;
        private bool _jumping;
        private float _jumpHeight;
        private CapsuleCollider _objectCapsule;
        private bool _isPlayerGrounded;

        private void Awake()
        {
            _objectRigidbody = GetComponent<Rigidbody>();
            _objectCapsule = GetComponent<CapsuleCollider>();
            _objectRigidbody.constraints = RigidbodyConstraints.FreezeRotationX |
                                           RigidbodyConstraints.FreezeRotationZ;
        }

        public void Move(Vector3 direction)
        {
            _objectRigidbody.MovePosition(transform.position + direction.normalized);
            _isPlayerGrounded = FloorDownTest(transform.position + Vector3.down * 0.5f);
            if (_jumping && _isPlayerGrounded)
                _objectRigidbody.AddForce(new Vector3(0f, _jumpHeight, 0f), ForceMode.Impulse);
            _jumping = false;
        }

        public void Jump(bool jumping)
        {
            _jumping = jumping;
        }

        public void Initialize(bool useGravity = false, float jumpHeight = 0f)
        {
            _objectRigidbody.useGravity = useGravity;
            _jumpHeight = jumpHeight;
        }

        private bool FloorDownTest(Vector3 direction)
        {
            RaycastHit hit;
            if (Physics.CapsuleCast(direction, direction, _objectCapsule.radius, Vector3.down, out hit, 0.1f))
                if (Vector3.Dot(hit.normal, Vector3.up) > 0.5f)
                    return true;
            return false;
        }
    }
}
