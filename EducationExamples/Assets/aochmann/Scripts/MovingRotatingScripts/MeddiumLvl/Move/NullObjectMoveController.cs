﻿using UnityEngine;

namespace Assets.aochmann.Scripts.MovingScripts.Move
{
    public class NullObjectMoveController : MonoBehaviour, IMove
    {
        public void Move(Vector3 direction) { }
        public void Jump(bool jumping) { }
        public void Initialize(bool useGravity = false, float jumpHeight = 0) { }
    }
}