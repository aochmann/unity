﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovementController : MonoBehaviour
{
    [SerializeField, Range(0f, 200f)]
    private float _movementSpeed = 2f;

    [SerializeField, Range(0f, 200f)]
    private float _rotationSpeed = 2f;

    [SerializeField, Range(0f, 200f)]
    private float _forcePower = 100f;

    [SerializeField, Range(0, 200)]
    private int _rayDirectionLength = 10;

    private Rigidbody _playerRigidbody;


    // Use this for initialization
    private void Start()
    {
        _playerRigidbody = GetComponent<Rigidbody>();


    }

    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled
    public void FixedUpdate()
    {
        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");

        Vector3 tmpDirection = new Vector3(hAxis, 0f, vAxis);

        Vector3 movement = tmpDirection * _movementSpeed * Time.fixedDeltaTime;
        Quaternion targetRotation = Quaternion.LookRotation(tmpDirection, Vector3.up);
        Quaternion newRotation = Quaternion.Lerp(_playerRigidbody.rotation, targetRotation, _rotationSpeed * Time.fixedDeltaTime);

        _playerRigidbody.MovePosition(transform.position + movement);
        _playerRigidbody.MoveRotation(newRotation);
        

    }
}
