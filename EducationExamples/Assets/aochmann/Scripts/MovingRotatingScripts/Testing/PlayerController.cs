﻿using System;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{

    [SerializeField, Range(0f, 200f)]
    private readonly float _moveSpeed = 8f;
        
    private Vector3 _targetPosition;
    private RaycastHit _rayHit;
    private Rigidbody _playerRigidbody;


    // Use this for initialization
    private void Start()
    {
        //_CharacterController = GetComponent<CharacterController>();
        _playerRigidbody = GetComponent<Rigidbody>();
        _playerRigidbody.velocity = Vector3.zero;
        _targetPosition = transform.position;

        _playerRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }

    // This function is called every fixed framerate frame, if the MonoBehaviour is enabled
    public void FixedUpdate()
    {
        Vector3 characterPosition = transform.position;

        Debug.DrawLine(characterPosition, _targetPosition, Color.black);

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePosition = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay(mousePosition);

            if (Physics.Raycast(ray, out _rayHit, Mathf.Infinity))
            {
                _targetPosition = _rayHit.point;
                _targetPosition.y = characterPosition.y;
                Vector3 tmp = _targetPosition - characterPosition;
                //tmp.y = 0;
                transform.rotation = Quaternion.LookRotation(tmp, Vector3.up);

            } //Raycast-if
        } //GetMouseButtonDown-if


        Vector3 heading = _targetPosition - characterPosition;
        heading.y = 0;

        float distance = Math.Abs(heading.magnitude) < 0.1f ? 1 : heading.magnitude;

        Vector3 direction = heading / distance;
        direction.y = 0;

        float d = direction.magnitude;

        float speed = Time.fixedDeltaTime * distance * _moveSpeed;
        _playerRigidbody.angularVelocity = Vector3.zero;
        //_PlayerRigidbody.Sleep();

        if (Math.Abs(_playerRigidbody.velocity.magnitude) < 0.1f)
        {
            _playerRigidbody.AddForce(direction * speed, ForceMode.Impulse);
        }
        else
            _playerRigidbody.AddForce(direction * speed);


        if (d <= 0.2f)
        {
            _playerRigidbody.velocity = Vector3.zero;
        }
    }
}