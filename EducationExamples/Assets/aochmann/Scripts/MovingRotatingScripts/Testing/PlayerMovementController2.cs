﻿using System;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovementController2 : MonoBehaviour
{
    [SerializeField, Range(0f, 2000f)]
    private float _moveSpeed = 8;

    [SerializeField, Range(0f, 2000f)]
    private float _rotationSpeed = 8;

    [SerializeField, Range(0f, 100f)]
    private float _jumpHeight = 12;

    [SerializeField, Range(0f, 100f)]
    private float _capsuleRadius = 0.45f;

    [SerializeField]
    private bool _isPlayerGrounded = false;

    private Rigidbody _playerRigidbody;

    private float _gravity;


    private void Start()
    {
        _playerRigidbody = GetComponent<Rigidbody>();
        _playerRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        _playerRigidbody.useGravity = false;
        _gravity = Physics.gravity.y;
    }

    private void FixedUpdate()
    {
        _isPlayerGrounded = false;
        Vector3 playerActualVelocity = _playerRigidbody.velocity;

        Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        Vector3 input = transform.TransformDirection(direction);

        if (input.magnitude > 1)
            input.Normalize();


        FloorDownTest(transform.position + Vector3.down * 0.5f);

        if (_isPlayerGrounded)
        {
            playerActualVelocity = input * _moveSpeed * Time.fixedDeltaTime;
            playerActualVelocity.y = _gravity * Time.fixedDeltaTime;

            if (Input.GetButtonDown("Jump"))
            {
                playerActualVelocity.y = _jumpHeight;
                _isPlayerGrounded = false;
            }
        }
        else
        {
            playerActualVelocity.x = Mathf.Clamp(playerActualVelocity.x + input.x  * _moveSpeed, -_moveSpeed, _moveSpeed);
            playerActualVelocity.y = Mathf.Clamp(playerActualVelocity.y + _gravity * Time.fixedDeltaTime, _gravity, _jumpHeight);
            playerActualVelocity.z = Mathf.Clamp(playerActualVelocity.z + input.z  * _moveSpeed, -_moveSpeed, _moveSpeed);
        }

        //Quaternion targetRotation = Quaternion.LookRotation(direction, Vector3.up);
        //Quaternion newRotation = Quaternion.Lerp(_playerRigidbody.rotation, targetRotation, _rotationSpeed * Time.fixedDeltaTime);

        _playerRigidbody.velocity = playerActualVelocity;// * Time.deltaTime
        //_playerRigidbody.MoveRotation(newRotation);
    }

    private void FloorDownTest(Vector3 direction)
    {
        RaycastHit hit;
        if (Physics.CapsuleCast(direction, direction, _capsuleRadius, Vector3.down, out hit, 0.1f))
            if (Vector3.Dot(hit.normal, Vector3.up) > 0.5f)
                _isPlayerGrounded = true;
    }
}
