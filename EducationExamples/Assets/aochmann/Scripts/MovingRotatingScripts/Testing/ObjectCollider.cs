﻿using UnityEngine;


public class ObjectCollider : MonoBehaviour
{
    private ContactPoint[] _patrolPoints;
    private Vector3 _currentObjectPosition;
    private Vector3 _objectVeliocity;
    private Renderer _objectRendererComponent;

    private float Red
    {
        get { return Random.Range(0f, 1f); }
    }

    private float Green
    {
        get { return Random.Range(0f, 1f); }
    }

    private float Blue
    {
        get { return Random.Range(0f, 1f); }
    }


    // Use this for initialization
    private void Start()
    {
        _patrolPoints = new ContactPoint[0];
        _objectRendererComponent = GetComponent<Renderer>();
        _objectRendererComponent.material.color = GenerateRandomColor();
    }

    private Color GenerateRandomColor()
    {
        return new Color(Red, Green, Blue);
    }

    private void OnCollisionEnter(Collision collision)
    {
        _patrolPoints = collision.contacts;
        _currentObjectPosition = transform.position;
        _objectVeliocity = collision.relativeVelocity;

        GameObject colliderObject = collision.gameObject;
        Vector3 colliderPosition = colliderObject.transform.position;
        Rigidbody colliderRigidbody = colliderObject.GetComponent<Rigidbody>();
        Renderer colliderRenderer = colliderObject.GetComponent<Renderer>();


        colliderRenderer.material.color = _objectRendererComponent.material.color;

        Vector3 normal = collision.contacts[0].normal;
        Vector3 force = -1 * normal * collision.relativeVelocity.magnitude;
        force.y = 0;

        colliderRigidbody.AddForce(force, ForceMode.Impulse);
        print(string.Format("{0}\t-\t{1}", collision.relativeVelocity, collision.relativeVelocity.magnitude));
    }

    // OnCollisionExit is called when this collider/rigidbody has stopped touching another rigidbody/collider
    private void OnCollisionExit(Collision collision)
    {
        _objectRendererComponent.material.color = GenerateRandomColor();
    }

    private void OnDrawGizmos()
    {
        if (_patrolPoints == null || _currentObjectPosition == null) return;
        foreach (ContactPoint point in _patrolPoints)
        {
            Vector3 currentObjectPosition = _currentObjectPosition;
            currentObjectPosition.y = point.point.y;

            Gizmos.color = Color.green;
            Gizmos.DrawLine(currentObjectPosition, point.point);

            Gizmos.color = Color.red;
            Gizmos.DrawRay(point.point, Vector3.Reflect(point.point - currentObjectPosition, point.normal) * 2);

            Gizmos.color = Color.magenta;
            Gizmos.DrawRay(point.point, Vector3.Reflect(point.point - _currentObjectPosition, point.normal) * 2);
        }
    }

    // OnTriggerEnter is called when the Collider other enters the trigger
    public void OnTriggerEnter(Collider other)
    {
        Debug.Log(string.Format("{0:0.00} <color=#FFC000 >{1}</color> {2}", Time.realtimeSinceStartup, "METHOD_NAME", "ADDITIONAL INFO"));
        GameObject colliderObject = other.gameObject;
        colliderObject.GetComponent<Renderer>().material.color = _objectRendererComponent.material.color;
    }

    // OnTriggerExit is called when the Collider other has stopped touching the trigger
    public void OnTriggerExit(Collider other)
    {
        Debug.Log(string.Format("{0:0.00} <color=#FFC000 >{1}</color> {2}", Time.realtimeSinceStartup, "METHOD_NAME", "ADDITIONAL INFO"));
        _objectRendererComponent.material.color = GenerateRandomColor();
    }

}
