﻿using UnityEngine;

public class ThirdExample : MonoBehaviour
{

    [SerializeField, Range(0f, 100f)] private float _sphereRadius = 2f;
	
	// Update is called once per frame
	private void Update ()
	{
	    RaycastHit hit;
	    if (Physics.SphereCast(transform.position, _sphereRadius, transform.forward, out hit) &&
	        hit.transform.tag == "Enemy")
	    {
	        print(hit.transform.name);
	    }
	    else
	    {
	        print("Enemy not detected");
	    }
	}

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, _sphereRadius);
    }


}