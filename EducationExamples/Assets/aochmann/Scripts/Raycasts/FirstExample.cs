﻿using UnityEngine;

public class FirstExample : MonoBehaviour
{
    [SerializeField, Range(0f, 1000f)] private float _maxRayDistance = 20f;

    private float _startTime;
    private long _hitsCount = 0;
    private RaycastHit _raycastHitObject;

    // Use this for initialization
    private void Start ()
	{
	    _startTime = Time.realtimeSinceStartup;
	}
	
	// Update is called once per frame
	private void Update ()
	{
        Debug.DrawRay(transform.position, transform.forward * _maxRayDistance);
        if (Physics.Raycast(transform.position, transform.forward, out _raycastHitObject, _maxRayDistance))
            if (_raycastHitObject.transform.tag.Equals("Enemy"))
            {
                //Do some logic
                Debug.Log(string.Format("Hit {0} at: {1}, object name: {2}", _hitsCount++,
                    Time.realtimeSinceStartup - _startTime, _raycastHitObject.transform.tag));
            }
    }
}