﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayExample : MonoBehaviour {
    [SerializeField, Range(0f, 1000f)] private float _maxRayDistance = 20f;


    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	    Debug.DrawRay(transform.position, transform.forward * _maxRayDistance);

	    RaycastHit rayHit;
	    if (Physics.Raycast(transform.position, transform.forward, out rayHit, _maxRayDistance))
	    {
	        if (rayHit.transform.tag == "Enemy")
	        {
	            print("enemy hit");
	        }
	    }
	}
}
