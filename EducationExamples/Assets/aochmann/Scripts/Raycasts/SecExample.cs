﻿using UnityEngine;

public class SecExample : MonoBehaviour 
{

    [SerializeField, Range(0f, 1000f)]
    private float _maxRayDistance = 20f;
    

    // Update is called once per frame
    private void Update()
    {
        Debug.DrawRay(transform.position, transform.forward * _maxRayDistance);
        Ray ray = new Ray(transform.position, transform.forward);
        int hitObjectCountWithExpectedTag = 0;
        foreach (RaycastHit raycastHit in Physics.RaycastAll(ray, _maxRayDistance))
        {
            if (raycastHit.transform.tag == "Enemy")
            {
                hitObjectCountWithExpectedTag++;
                Debug.Log((hitObjectCountWithExpectedTag).ToString() + " " + raycastHit.transform.tag + " " + raycastHit.transform.gameObject.name);
            }
        }
        
    }
}