﻿using UnityEngine;

namespace Assets.aochmann.Scripts.GUIs
{
    [ExecuteInEditMode]
    public class DisplayPlayerName : MonoBehaviour
    {
        [SerializeField] private GUIContent _playerName = new GUIContent("Player");
        [SerializeField, Range(0f, 100f)] private float _boxWidth = 20f;
        [SerializeField, Range(0f, 100f)] private float _boxHeight = 20f;
        [SerializeField,] private Vector2 _offset;
        [SerializeField, Range(0f, 2)] private float _xMultiplier = 0.1f;
        [SerializeField, Range(0f, 2)] private float _yMultiplier = 0.5f;

        // OnGUI is called for rendering and handling GUI events
        private void OnGUI()
        {
            var boxPosition = Camera.main.WorldToScreenPoint(transform.position);
            boxPosition.y = Screen.height - boxPosition.y;
            boxPosition.x -= _boxWidth * _xMultiplier;
            boxPosition.y -= _boxHeight * _yMultiplier;

            Vector2 content = UnityEngine.GUI.skin.box.CalcSize(_playerName);
        
            UnityEngine.GUI.Box(new Rect(boxPosition.x - (content.x ) / 2f + _offset.x, boxPosition.y - _offset.y, _boxWidth + content.x, _boxHeight + content.y), _playerName);
        }
    }
}