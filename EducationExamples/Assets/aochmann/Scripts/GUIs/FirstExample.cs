﻿using UnityEngine;

namespace Assets.aochmann.Scripts.GUIs
{
   // [ExecuteInEditMode]
    public class FirstExample : MonoBehaviour
    {
        [SerializeField] private GUISkin _guiSkin;
        [SerializeField]
        private string _textFieldString;

        [SerializeField]
        private bool _toggleBool;

        private int _toolbarInt = 0;
        [SerializeField]
        private string[] _toolbarStrings = { "Toolbar1", "Toolbar2", "Toolbar3" };

        private Rect _windowRect = new Rect(Screen.width - 200, Screen.height / 2f - 100f, 200, 200);
        private Vector2 _scrollViewVector = Vector2.zero;

        private string _innerText = "I am inside the ScrollView";

        [SerializeField, Range(0, 10)]
        private int _columnsCount = 2;

        private int iteracja = 0;

        private GUIStyle style;
        private bool test;
        private void Awake()
        {
            //GUI.skin = _guiSkin;
            style = _guiSkin.GetStyle("klucz");
            test = true;
        }





        // OnGUI is called for rendering and handling GUI events
        private void OnGUI()
        {
            if (!test) return;

            GUI.Box(new Rect(Screen.width / 2f - 50f, Screen.height/2f -50f, 100f, 100f),"Some Box", style );

            //GUI.BeginGroup(new Rect(0f, 200f, Screen.width, 20f));
            //{
            //   GUILayout.BeginHorizontal();
            //    {
            //        bool result = GUILayout.Button("Asdasd");

            //        if (result)
            //        {
            //            Debug.Log(iteracja++);
            //        }

            //        if (GUILayout.RepeatButton("Asdasd"))
            //        {
            //            Debug.Log(iteracja++);
            //        }
            //        GUILayout.TextField("Some Text Field");
            //    }
            //    GUILayout.EndHorizontal();
            //}
            //GUI.EndGroup();

            //if (GUILayout.Button("Click me"))
            //    Debug.Log("1");

            //if (UnityEngine.GUI.Button(new Rect(200, 0, 200, 40), "Click me 2"))
            //    Debug.Log("2");

            //if (Time.time % 2 < 1)
            //    if (GUI.Button(new Rect(10, 120, 200, 20), "Meet the flashing button"))
            //        print("You clicked me!");


            GUI.Box(new Rect(50, 200, 50, 50), "Box");
            //GUI.Box(new Rect(Screen.width - 100, 0, 100, 50), "Top-right");
            //GUI.Label(new Rect(0, 55, 100, 50), "This is the text string for a Label Control");
            //if (GUI.RepeatButton(new Rect(5, 25, 100, 30), "RepeatButton"))
            //{
            //    Debug.Log(Time.realtimeSinceStartup);
            //}

            //_textFieldString = GUI.TextField(new Rect(5, 150, 100, 30), _textFieldString);
            //_toggleBool = GUI.Toggle(new Rect(200, 50, 100, 30), _toggleBool, "Toggle");


            //_toolbarInt = GUI.Toolbar(new Rect(0, Screen.height - 30, 250, 30), _toolbarInt, _toolbarStrings);

            //_toolbarInt = GUI.SelectionGrid(new Rect(Screen.width - 300, Screen.height - 60, 300, 60), _toolbarInt, _toolbarStrings, _columnsCount);

            //_windowRect = GUI.Window(0, _windowRect, WindowFunction, "My Window");

            //_scrollViewVector = GUI.BeginScrollView(new Rect(300, 200, 100, 100), _scrollViewVector, new Rect(0, 0, 400, 400));
            //{
            //    _innerText = GUI.TextArea(new Rect(0, 0, 400, 400), _innerText);
            //}
            //GUI.EndScrollView();
        }

        private void WindowFunction(int windowID)
        {
            switch (windowID)
            {
                case 0:
                    _toggleBool = GUILayout.Toggle(_toggleBool, "Toggle");
                    GUI.DragWindow();
                    break;

            }
        }

    }
}