﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.aochmann.Scripts.GUIs
{
    public class SystemTime : MonoBehaviour
    {
        private Text _text;

        private void Awake()
        {
            _text = GetComponent<Text>();
            if (_text == null)
                throw new NullReferenceException("Missing Text Component");

            _text.text = DateTime.Now.ToString("HH:mm:ss");
        }

        // Update is called once per frame
        private void Update ()
        {
            _text.text = DateTime.Now.ToString("HH:mm:ss");
        }
    }
}