﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.aochmann.Scripts.GUIs
{
    public class SliderDisplayer : MonoBehaviour
    {
        [SerializeField] private Text _text;
        private Slider _slider;

        private void Awake()
        {
            _slider = GetComponent<Slider>();
        }

        private void Start()
        {
            if (_text != null && _slider != null)
            {
                _text.text = _slider.value.ToString();
                _slider = null;
            }
        }

        public void ChangeValue(float value)
        {
            if (_text != null)
            {
                _text.text = value.ToString();
            }
        }
    }
}