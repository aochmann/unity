﻿public enum EnemyState
{
    Patrol,
    Chase,
    Alert
}