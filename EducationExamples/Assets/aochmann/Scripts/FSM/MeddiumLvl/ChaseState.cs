using Task5.FSM;
using UnityEngine;

namespace Assets.aochmann.Scripts.FSM
{
    public class ChaseState : IState
    {
        private readonly IStateMachine _stateMachine;

        public ChaseState(IStateMachine statePatternMachine)
        {
            _stateMachine = statePatternMachine;
        }

        public void UpdateState()
        {
            Look();
            Chase();
        }

        public void OnTriggerEnter(Collider other)
        {
        }

        private void ChangeState(EnemyState state)
        {
            if (state == EnemyState.Chase)
            {
                Debug.Log("Can't transition to same state");
                return;
            }

            _stateMachine.ChangeState(state);
        }

        private void Look()
        {
            RaycastHit hit;
            Vector3 tmpEyesPosition = _stateMachine.GetEyesPosition().position;

            Vector3 enemyToTarget = (_stateMachine.GetChaseObject() + _stateMachine.GetObjectOffset()) - tmpEyesPosition;

            if (Physics.Raycast(tmpEyesPosition, enemyToTarget, out hit, _stateMachine.GetObjectSightRange()) && hit.collider.CompareTag("Player"))
            {
                _stateMachine.SetChaseObject(hit.transform);
            }
            else
            {
                ChangeState(EnemyState.Alert);
            }
        }

        private void Chase()
        {
            _stateMachine.SetObjectMeshRendererFlagColor(Color.red);
            _stateMachine.SetObjectDestinationPosition(_stateMachine.GetChaseObject());
        }


    }
}