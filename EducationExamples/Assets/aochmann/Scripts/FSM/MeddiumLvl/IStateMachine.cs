﻿using UnityEngine;

namespace Task5.FSM
{
    public interface IStateMachine
    {
        void ChangeState(EnemyState newState);
        Vector3 GetChaseObject();
        Transform GetEyesPosition();
        Vector3 GetObjectOffset();
        float GetObjectSightRange();
        void SetObjectMeshRendererFlagColor(Color color);
        void SetObjectDestinationPosition(Vector3 position);
        void SetChaseObject(Transform objecTransform);
        void StopObjectMoving();
        void RotateObject();
        float GetMaxSearchingTime();
        int GetWayPointsCount();
        Vector3 GetWayPoint(int index);
        UnityEngine.AI.NavMeshAgent GetNavmesh();

    }
}