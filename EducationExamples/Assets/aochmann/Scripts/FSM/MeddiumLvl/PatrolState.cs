﻿using Task5.FSM;
using UnityEngine;

namespace Assets.aochmann.Scripts.FSM
{
    public class PatrolState : IState
    {
        private readonly IStateMachine _stateMachine;
        private int nextWayPoint;

        public PatrolState(IStateMachine statePatternMachine)
        {
            _stateMachine = statePatternMachine;
        }

        public void UpdateState()
        {
            Look();
            Patrol();
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
                ChangeState(EnemyState.Alert);
        }

        private void ChangeState(EnemyState state)
        {
            if (state == EnemyState.Patrol)
            {
                Debug.LogError("Not allowed");
                return;
            }

            _stateMachine.ChangeState(state);
        }

        private void Look()
        {
            RaycastHit hit;
            Transform eyesPosition = _stateMachine.GetEyesPosition();
            if (Physics.Raycast(eyesPosition.position, eyesPosition.forward, out hit, _stateMachine.GetObjectSightRange()) && hit.collider.CompareTag("Player"))
            {
                _stateMachine.SetChaseObject(hit.transform);
                ChangeState(EnemyState.Chase);
            }
        }

        private void Patrol()
        {

            _stateMachine.SetObjectMeshRendererFlagColor(Color.green);
            _stateMachine.SetObjectDestinationPosition(_stateMachine.GetWayPoint(nextWayPoint));

            UnityEngine.AI.NavMeshAgent tmpAgent = _stateMachine.GetNavmesh();

            if (tmpAgent.remainingDistance <= tmpAgent.stoppingDistance && !tmpAgent.pathPending)
            {
                nextWayPoint = (nextWayPoint + 1) % _stateMachine.GetWayPointsCount();

            }


        }
    }
}
