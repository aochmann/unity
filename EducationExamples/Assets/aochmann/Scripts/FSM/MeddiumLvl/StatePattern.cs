﻿using System;
using System.Collections.Generic;
using Task5.FSM;
using UnityEngine;

namespace Assets.aochmann.Scripts.FSM
{
    public class StatePattern : MonoBehaviour, IStateMachine
    {

        [SerializeField]private float _searchingTurnSpeed = 120f;
        [SerializeField]private float _searchingDuration = 4f;
        [SerializeField]private float _sightRange = 20f;
        [SerializeField]private Transform _wayPointObject;
        [SerializeField]private Transform _eyes;
        [SerializeField]private readonly Vector3 _offset = new Vector3(0, 0.5f, 0);
        [SerializeField]private MeshRenderer _meshRendererFlag;

        private IState _currentState;
        private UnityEngine.AI.NavMeshAgent _navMeshAgent;
        private Vector3[] _wayPoints;

        private readonly Dictionary<EnemyState, IState> _fsmStates = new Dictionary<EnemyState, IState>();

        [HideInInspector]
        public Transform chaseTarget;


        private void Awake()
        {
            _wayPoints = new Vector3[_wayPointObject.childCount];

            for (int i = 0; i < _wayPointObject.childCount; i++)
            {
                _wayPoints[i] = _wayPointObject.GetChild(i).position;
            }

            _fsmStates[EnemyState.Alert] = new AlertState(this);
            _fsmStates[EnemyState.Patrol] = new PatrolState(this);
            _fsmStates[EnemyState.Chase] = new ChaseState(this);

            _navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        }

        // Use this for initialization
        private void Start()
        {
            _currentState = _fsmStates[EnemyState.Patrol];
        }

        // Update is called once per frame
        private void Update()
        {
            _currentState.UpdateState();
        }

        private void OnTriggerEnter(Collider other)
        {
            _currentState.OnTriggerEnter(other);
        }

        public void ChangeState(EnemyState newState)
        {
            if (!_fsmStates.ContainsKey(newState))
            {
                throw new Exception("Not implemented state class");
            }

            _currentState = _fsmStates[newState];
        }

        public Vector3 GetChaseObject()
        {
            return chaseTarget.position;
        }

        public Transform GetEyesPosition()
        {
            return _eyes;
        }

        public Vector3 GetObjectOffset()
        {
            return _offset;
        }

        public float GetObjectSightRange()
        {
            return _sightRange;
        }

        public void SetObjectMeshRendererFlagColor(Color color)
        {
            _meshRendererFlag.material.color = color;
        }

        public void SetObjectDestinationPosition(Vector3 position)
        {
            _navMeshAgent.destination = position;
            _navMeshAgent.Resume();
        }

        public void SetChaseObject(Transform objectTransform)
        {
            chaseTarget = objectTransform;
        }

        public void StopObjectMoving()
        {
            _navMeshAgent.Stop();
        }

        public void RotateObject()
        {
            transform.Rotate(0, _searchingTurnSpeed * Time.deltaTime, 0);

        }

        public float GetMaxSearchingTime()
        {
            return _searchingDuration;
        }

        public int GetWayPointsCount()
        {
            return _wayPoints.Length;
        }

        public Vector3 GetWayPoint(int index)
        {
            if(index >= _wayPoints.Length) throw new IndexOutOfRangeException();
            return _wayPoints[index];
        }

        public UnityEngine.AI.NavMeshAgent GetNavmesh()
        {
            return _navMeshAgent;
        }
    }


}
