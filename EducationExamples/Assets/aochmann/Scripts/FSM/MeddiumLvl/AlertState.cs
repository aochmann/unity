﻿using Task5.FSM;
using UnityEngine;

namespace Assets.aochmann.Scripts.FSM
{
    public class AlertState : IState
    {
        private readonly IStateMachine _stateMachine;
        private float _searchTimer;

        public AlertState(IStateMachine statePatternMachine)
        {
            _stateMachine = statePatternMachine;
        }


        public void UpdateState()
        {
            Look();
            Search();
        }

        public void OnTriggerEnter(Collider other)
        {
        }

        private void ChangeState(EnemyState state)
        {
            if (state == EnemyState.Alert)
            {
                Debug.Log("Can't transition to same state");
                return;
            }

            _stateMachine.ChangeState(state);
            _searchTimer = 0f;
        }

        private void Look()
        {
            RaycastHit hit;
            Transform tmpEyes = _stateMachine.GetEyesPosition();

            if (Physics.Raycast(tmpEyes.position, tmpEyes.forward, out hit, _stateMachine.GetObjectSightRange()) && hit.collider.CompareTag("Player"))
            {
                _stateMachine.SetChaseObject(hit.transform);
                ChangeState(EnemyState.Chase);
            }
        }

        private void Search()
        {
            _stateMachine.SetObjectMeshRendererFlagColor(Color.yellow);
            _stateMachine.SetObjectDestinationPosition(_stateMachine.GetChaseObject());
            _stateMachine.StopObjectMoving();
            _stateMachine.RotateObject();

            _searchTimer += Time.deltaTime;

            if (_searchTimer >= _stateMachine.GetMaxSearchingTime())
                ChangeState(EnemyState.Patrol);
        }
    }
}