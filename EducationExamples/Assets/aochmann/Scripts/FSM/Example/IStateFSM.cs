namespace Assets.aochmann.Scripts.FSM.Example
{
    public interface IStateFSM
    {
        void UpdateMachine();
    }

    public interface IStateLogic
    {
        void ChangeMachineState(StatesFSM newState);
    }

    public interface IControllManager
    {
        void ChangeFSMState(StatesFSM state);
    }
}