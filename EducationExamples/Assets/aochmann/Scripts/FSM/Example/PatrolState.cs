﻿namespace Assets.aochmann.Scripts.FSM.Example
{
    public class PatrolState : IStateFSM, IStateLogic
    {
        private IControllManager _fsmManager;

        public PatrolState(IControllManager manager)
        {
            _fsmManager = manager;
        }

        public void UpdateMachine()
        {
            //tataj znajduje się logika Patrolowania
            /*

if(enemy was seen){
 ChangeMachineState(Walk);
}

*/
        }

        public void ChangeMachineState(StatesFSM newState)
        {
            _fsmManager.ChangeFSMState(newState);
        }
    }
}