﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.aochmann.Scripts.FSM.Example
{
    public class NewBehaviourScript : MonoBehaviour, IControllManager
    {
        private IStateFSM _currenState;
        private readonly Dictionary<StatesFSM, IStateFSM> _fsmStates
            = new Dictionary<StatesFSM, IStateFSM>();

        private void Awake()
        {
            _currenState = _fsmStates[StatesFSM.Patrol] = new PatrolState(this);
            _fsmStates[StatesFSM.Patrol] = new PatrolState(this);
            _fsmStates[StatesFSM.Patrol] = new PatrolState(this);
            _fsmStates[StatesFSM.Patrol] = new PatrolState(this);
            _fsmStates[StatesFSM.Patrol] = new PatrolState(this);


        }

        // Update is called once per frame
        void Update()
        {
            _currenState.UpdateMachine();
        }

        public void ChangeFSMState(StatesFSM state)
        {
            _currenState = _fsmStates[state];
        }
    }
}