﻿namespace Assets.aochmann.Scripts.FSM.Example
{
    public enum StatesFSM
    {
        Patrol,
        Chase,
        Walk,
        Run,
        GoToBunker
    }
}