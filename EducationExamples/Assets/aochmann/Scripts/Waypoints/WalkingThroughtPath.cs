﻿using UnityEngine;

namespace Assets.aochmann.Scripts.Waypoints
{
    [RequireComponent(typeof(CharacterController))]
    public class WalkingThroughtPath : MonoBehaviour
    {
        [SerializeField]
        private Transform _waypointsContainer;

        [SerializeField]
        private bool _canGoBack = false;

        [SerializeField, Range(0f, 100f)]
        private float _speed = 2f;

        [SerializeField, Range(0f, 100f)]
        private float _lineLength = 10f;
        [SerializeField, Range(0f, 2f)]
        private float _stopDistance = 0.5f;


        [SerializeField, Range(0f, 100f)] private float _rotationSpeed = 10f;

        private Vector3[] _points;

        [SerializeField]
        private int _goingPointIndex = 0;

        private bool _endTrace = false;
        private CharacterController _characterController;

        public void Awake()
        {
            _characterController = GetComponent<CharacterController>();
            _points = new Vector3[_waypointsContainer.childCount];

            for (int i = 0; i < _points.Length; i++)
            {
                _points[i] = _waypointsContainer.GetChild(i).position;
            }
        }

        public void FixedUpdate()
        {
            Vector3 point = _points[_goingPointIndex];
            point.y = transform.position.y;

            if (Vector3.Distance(point, transform.position) < _stopDistance)
            {
                if (_canGoBack && _endTrace)
                {
                    _goingPointIndex--;

                    if (_goingPointIndex < 0)
                    {
                        _goingPointIndex = 0;
                        _endTrace = false;
                    }
                }

                if (_endTrace == false)
                {
                    if (!_canGoBack)
                        _goingPointIndex = (_goingPointIndex + 1)%_points.Length;
                    else if (_goingPointIndex == _points.Length - 1)
                    {
                        _endTrace = true;
                        return;
                    }
                    else
                        _goingPointIndex = (_goingPointIndex + 1)%_points.Length;
                }
            }

            Vector3 hedding = point - transform.position;
            Vector3 direction = hedding.normalized;
            Debug.DrawLine(transform.position, transform.position + direction * _lineLength);

            var qTo = Quaternion.LookRotation(direction);

            transform.rotation = Quaternion.Slerp(transform.rotation, qTo, _rotationSpeed * Time.deltaTime);

            _characterController.Move(direction * _speed * Time.fixedDeltaTime);
        }
    }
}