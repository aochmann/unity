﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.aochmann.Scripts.Waypoints
{
    [ExecuteInEditMode]
    public class WaypointSystem : MonoBehaviour
    {
        private Transform _waypointContainer;

        [SerializeField]
        private Color _pointsColor = Color.magenta;

        [SerializeField]
        private Color _linesColor = Color.green;

        [SerializeField]
        private bool _returnPath = false;

        [SerializeField, Range(0.1f, 10f)]
        private float _pointRadius = 2f;



        private void Awake()
        {
            _waypointContainer = transform;


            //if (_waypointContainer == null)
            //{
            //    //implementing error handling/warning
            //}
            //else
            //{
            //    if (_waypointContainer.childCount == 0)
            //    {
            //        //Implementing warning msg
            //    }
            //}
        }

        // Implement this OnDrawGizmosSelected if you want to draw gizmos only if the object is selected
        public void OnDrawGizmos()
        {
            if (_waypointContainer != null)
            {
                Queue queueList = new Queue();
                List<Transform> pointsList = new List<Transform>();

                queueList.Enqueue(_waypointContainer);


                while (queueList.Count > 0)
                {
                    Transform pointP = queueList.Dequeue() as Transform;

                    if(pointP == null) continue;

                    if ( pointP.childCount > 0)
                        for (int i = 0; i < pointP.childCount; i++)
                            queueList.Enqueue(pointP.GetChild(i));
                    else
                        pointsList.Add(pointP);
                }

                for (int i = 0; i < pointsList.Count; i++)
                {
                    Vector3 currentPoint = pointsList[i].position;
                    Gizmos.color = _pointsColor;
                    Gizmos.DrawSphere(currentPoint, _pointRadius);
                    Gizmos.color = _linesColor;

                    if (i > 0)
                    {
                        Vector3 previousePoint = pointsList[i - 1].position;
                        Gizmos.DrawLine(currentPoint, previousePoint);
                    }

                    if (i == pointsList.Count - 1 && _returnPath == true)
                    {
                        Vector3 previousePoint = pointsList[0].position;
                    
                        Gizmos.DrawLine(currentPoint, previousePoint);
                    }
                }



            }
        }
    }
}
