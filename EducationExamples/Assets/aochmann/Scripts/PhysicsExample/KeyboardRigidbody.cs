﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class KeyboardRigidbody : MonoBehaviour
{
    private Rigidbody _objectRigidbody;

    [SerializeField, Range(0f, 500f)]
    private float _forcePower = 4f;
    // Use this for initialization
    private void Awake ()
	{
	    _objectRigidbody = GetComponent<Rigidbody>();
	    _objectRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
	    _objectRigidbody.useGravity = false;

	}
	
	// Update is called once per frame
	private void Update () 
	{
	    if (Input.GetKey(KeyCode.W))
	    {
	        _objectRigidbody.AddForce(Vector3.forward * _forcePower);
	    }

        if (Input.GetKey(KeyCode.S))
        {
            _objectRigidbody.AddForce(Vector3.back * _forcePower);
        }

	    if (Input.GetKey(KeyCode.A))
	    {
            _objectRigidbody.AddForce(Vector3.left * _forcePower);
        }

        if (Input.GetKey(KeyCode.D))
        {
            _objectRigidbody.AddForce(Vector3.right * _forcePower);
        }
    }
}