﻿using UnityEngine;

namespace Assets.aochmann.Scripts.PhysicsExample
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerKeyboardSimpleMove : MonoBehaviour
    {
        private Rigidbody _objectRigidbody;
        private bool _isPlayerGrounded;
        [SerializeField, Range(0f, 100f)] private float _groundDistance = 1f;
        [SerializeField, Range(0f, 2f)] private float _stopDistance = 0.1f;
        [SerializeField, Range(0f, 100f)] private float _force = 0.1f;
        [SerializeField] private ForceMode _forceMode = ForceMode.Force;
        [SerializeField, Range(0f, 100f)]
        private float _gravityMultiplier = 3f;

        private bool _jumping;
        [SerializeField, Range(0f, 100f)]
        private float _jumpingHeight;

        public void Awake()
        {
            _objectRigidbody = GetComponent<Rigidbody>();
            _objectRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }

        public void FixedUpdate()
        {
           var input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));

            _objectRigidbody.AddForce(input.normalized * _force);
        }

        private bool FloorDownTest()
        {
            return Physics.Raycast(transform.position, -Vector3.up, _groundDistance + 0.1f);
        }

        private Vector3 GetInputDirection()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            return vertical * Vector3.forward + horizontal * Vector3.right;
        }
    }
}