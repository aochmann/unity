﻿using UnityEngine;

namespace Assets.aochmann.Scripts.PhysicsExample
{
    [RequireComponent(typeof(Rigidbody))]
    public class BilliardBallClick : MonoBehaviour
    {
        [SerializeField] private bool _useTorque = true;
        [SerializeField, Range(0f, 1000f)] private float _forcePower = 10f;
        [SerializeField, Range(0f, 100f)] private float _maxAngularVelocity = 25f;
        [SerializeField] private ForceMode _forceMode = ForceMode.Impulse;

        private Rigidbody _objectRigidbody;

        private void Awake()
        {
            _objectRigidbody = GetComponent<Rigidbody>();
            _objectRigidbody.maxAngularVelocity = _maxAngularVelocity;
        }

        public void OnMouseDown()
        {
            Vector3 direction = (Vector3.forward*-1f).normalized; 
            Debug.Log(direction);
            if (_useTorque)
            {
                _objectRigidbody.AddTorque(new Vector3(direction.z, 0f, -direction.x)
                                            * _forcePower, _forceMode);
            }
            else
            {
                _objectRigidbody.AddForce(direction * _forcePower, _forceMode);
            }
        }
    }
}