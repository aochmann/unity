﻿using UnityEngine;

namespace Assets.aochmann.Scripts.PhysicsExample
{
    [RequireComponent(typeof(Rigidbody))]
    public class ForceShoot : MonoBehaviour
    {
        [SerializeField, Range(0f, 1000f)]
        private float _forcePower = 100f;

        [SerializeField]
        private ForceMode _forceMode = ForceMode.Impulse;

        private Rigidbody _objectRigidbody;

        public void Awake()
        {
            _objectRigidbody = GetComponent<Rigidbody>();
        }

        public void OnMouseDown()
        {
            _objectRigidbody.AddForce(transform.forward * _forcePower, _forceMode);
            _objectRigidbody.useGravity = true;
        }
    }
}