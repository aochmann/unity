﻿using UnityEngine;

namespace Assets.aochmann.Scripts.PhysicsExample
{
    public class BillardKeyboardClick : MonoBehaviour
    {

        [SerializeField]
        private bool _useTorque = true;
        [SerializeField, Range(0f, 1000f)]
        private float _forcePower = 10f;
        [SerializeField, Range(0f, 100f)]
        private float _maxAngularVelocity = 25f;
        [SerializeField]
        private ForceMode _forceMode = ForceMode.Impulse;

        private Rigidbody _objectRigidbody;
        private Vector3 _moveDirection;

        private void Awake()
        {
            _objectRigidbody = GetComponent<Rigidbody>();
            _objectRigidbody.maxAngularVelocity = _maxAngularVelocity;
        }

        private void Update()
        {
            float vertical = Input.GetAxis("Vertical");
            if (vertical >= 0)
                vertical = 0f;

            _moveDirection = (vertical * Vector3.forward).normalized;
        }

        public void FixedUpdate()
        {
            if (_useTorque)
            {
                _objectRigidbody.AddTorque(new Vector3(_moveDirection.z, 0f, -_moveDirection.x)
                                           * _forcePower, _forceMode);
            }
            else
            {
                _objectRigidbody.AddForce(_moveDirection * _forcePower, _forceMode);
            }
        }
    }
}