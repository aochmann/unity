﻿using UnityEngine;

namespace Assets.aochmann.Scripts.PhysicsExample
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerClickSimpleMove : MonoBehaviour
    {
        [SerializeField, Range(0f, 200f)]
        private float _movementSpeed = 2f;

        [SerializeField, Range(0f, 200f)]
        private float _rotationSpeed = 2f;

        [SerializeField, Range(0f, 200f)]
        private float _forcePower = 100f;

        [SerializeField, Range(0, 200)]
        private int _rayDirectionLength = 10;

        [SerializeField, Range(0, 200)]
        private float _stopDistance = 1.2f;


        [SerializeField, Range(0f, 100f)]
        private float _jumpHeight = 12;

        [SerializeField]
        private bool _isPlayerGrounded = false;

        [SerializeField, Range(0f, 100f)]
        private float _capsuleRadius = 0.45f;

        [SerializeField, Range(0f, 2f)]
        private float _groundDistance = 0.2f;


        private Rigidbody _objectRigidbody;
        private Vector3 _targetPosition;
        private RaycastHit _rayHit;
        private float _gravity;


        private void Start()
        {
            _objectRigidbody = GetComponent<Rigidbody>();
            _objectRigidbody.velocity = Vector3.zero;
            _targetPosition = transform.position;
            _objectRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            _objectRigidbody.useGravity = false;
            _gravity = Physics.gravity.y;
        }


        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _objectRigidbody.velocity = Vector3.zero;
                Vector3 characterPosition = transform.position;

                Vector3 mousePosition = Input.mousePosition;
                Ray ray = Camera.main.ScreenPointToRay(mousePosition);

                if (Physics.Raycast(ray, out _rayHit, Mathf.Infinity))
                {
                    _targetPosition = _rayHit.point;
                    _targetPosition.y = characterPosition.y;
                    Vector3 tmp = _targetPosition - characterPosition;
                    transform.rotation = Quaternion.LookRotation(tmp, Vector3.up);

                } //Raycast-if

            }
        }

        private void FixedUpdate()
        {
            Debug.DrawLine(transform.position, _targetPosition);
            _isPlayerGrounded = FloorDownTest();
            Vector3 playerActualVelocity = _objectRigidbody.velocity;

            if (Vector3.Distance(_targetPosition, transform.position) < _stopDistance)
            {
                _targetPosition = Vector3.zero;
                _objectRigidbody.velocity = Vector3.zero;
                return;

            }

            if (_targetPosition != transform.position && _targetPosition != Vector3.zero)
            {
                Vector3 hedding = _targetPosition - transform.position;
                _objectRigidbody.AddForce(_targetPosition.normalized * _movementSpeed);
            }

            if (_isPlayerGrounded)
            {
                //playerActualVelocity = input * _movementSpeed * Time.fixedDeltaTime;
                playerActualVelocity.y = _gravity * Time.fixedDeltaTime;

                if (Input.GetButtonDown("Jump"))
                {
                    playerActualVelocity.y = _jumpHeight;
                    _isPlayerGrounded = false;
                }
            }
            else
            {
                //playerActualVelocity.x = Mathf.Clamp(playerActualVelocity.x + input.x * _moveSpeed, -_moveSpeed, _moveSpeed);
                playerActualVelocity.y = Mathf.Clamp(playerActualVelocity.y + _gravity * Time.fixedDeltaTime, _gravity, _jumpHeight);
                //playerActualVelocity.z = Mathf.Clamp(playerActualVelocity.z + input.z * _moveSpeed, -_moveSpeed, _moveSpeed);
            }

            _objectRigidbody.velocity = playerActualVelocity;
        }

        private bool FloorDownTest()
        {
            return Physics.Raycast(transform.position, -Vector3.up, _groundDistance + 0.1f);
        }
    }
}